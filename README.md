# Tyristorstyring

Tyristoromformar for 1, og 3-fase.

## Spesifikasjonar

Minimumskrav

* 6 isolerte utgangar for gate signal
* LC-display for avlesing av styringsvinkel
* Potmeter, eller enkoder for justering av styringsvinkel
* Inngang for måling av nullgjennomgang i spenning

Ekstra funksjonar

* Fjernstyring via Modbus
* Måling av straum og spenning
* Berekning av RMS, PF, og effekt.
